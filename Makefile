.PHONY: build examples

PACKAGE_NAME:=logging_configurer
MODULES:=logging_configurer examples
PYTHON:=python3

all: dev style checks dists test

dev: clean-venv ensure-pipenv mk-venv pipenv-install-dev requirements

ensure-pipenv:
	$(PYTHON) -m pip install --user --upgrade 'pipenv==2018.11.26' 'pip==20.0.2'
	@echo "ensure your local python install is in your PATH"

pipenv-install-dev:
	pipenv install --dev --ignore-pipfile --deploy --three --python=$(PYTHON)
	pipenv run pip install -e .

mk-venv: clean-venv
	# use that to configure a symbolic link to the virtualenv in .venv
	mkdir -p .venv

clean-venv:
	@rm -fr .venv

install-local:
	pipenv install --ignore-pipfile --deploy

install-system:
	pipenv install --system

style: isort black

isort:
	pipenv run isort -rc -y $(MODULES)

black:
	pipenv run black $(MODULES)

black-check:
	pipenv run black --check $(MODULES)

checks: black-check flake8 pylint bandit pydocstyle

flake8:
	pipenv run python setup.py flake8

pylint:
	pipenv run pylint --rcfile=.pylintrc --output-format=colorized $(MODULES)

bandit:
	pipenv run bandit -c .bandit.yml -r $(MODULES)

pydocstyle:
	pipenv run pydocstyle $(MODULES)

build: dists

shell:
	pipenv shell

test:
	pipenv run pytest $(MODULES)

test-v:
	pipenv run pytest -vv $(MODULES)

test-coverage:
	pipenv run pytest --junitxml=testresults.xml -v --cov logging_configurer --cov-report html:coverage_html --cov-report term $(MODULES)

requirements:
	# needed until PBR supports `Pipfile`
	pipenv run pipenv_to_requirements

dists: requirements sdist wheels

sdist:
	pipenv run python setup.py sdist

wheels:
	pipenv run python setup.py bdist_wheel

pypi-publish: build
	pipenv run python setup.py upload -r arti

update: pipenv-update dev

pipenv-update:
	pipenv update --clear

githook: style requirements

sc: requirements style check

sct: requirements style check test

lock:
	pipenv lock

tag-pbr:
	@{ \
		set -e ;\
		export VERSION=$$(pipenv run python setup.py --version | cut -d. -f1,2,3); \
		echo "I: Computed new version: $$VERSION"; \
		echo "I: presse ENTER to accept or type new version number:"; \
		read VERSION_OVERRIDE; \
		VERSION=$${VERSION_OVERRIDE:-$$VERSION}; \
		PROJECTNAME=$$(pipenv run python setup.py --name); \
		echo "I: Tagging $$PROJECTNAME in version $$VERSION with tag: v$$VERSION" ; \
		echo "$$ git tag v$$VERSION -m \"$$PROJECTNAME $$VERSION\""; \
		git tag v$$VERSION -m "$$PROJECTNAME $$VERSION"; \
		echo "I: Pushing tag v$$VERSION, press ENTER to continue, C-c to interrupt"; \
		read _; \
		echo "$$ git push origin v$$VERSION"; \
		git push origin v$$VERSION; \
	}
	@# Note:
	@# To sign, need gpg configured and the following command:
	@#  git tag -s $$VERSION -m \"$$PROJECTNAME $$VERSION\""



push: githook
	git push origin --all
	git push origin --tags

publish: clean-dist dists
	twine upload -r arti --skip-existing dist/* || true

clean-dist:
	rm -rfv build dist/

clean: clean-dist clean-venv
	pipenv --rm || true
	find . -name '__pycache__'  -exec rm -rf {} \; || true
	find . -name '.cache'  -exec rm -rf {} \; || true
	find . -name "*.pyc" -exec rm -f {} \; || true
	rm -rf .mypy_cache || true
	rm -rf .pytest_cache *.egg-info || true
	rm -rf nestor/testenv || true

examples:
	@echo "========================================================================================"
	@echo "Plain output"
	pipenv run python examples/ex_plain.py
	@echo
	@echo "========================================================================================"
	@echo "Dev output"
	pipenv run python examples/ex_dev.py
	@echo
	@echo "========================================================================================"
	@echo "Dev output (uncolored)"
	pipenv run python examples/ex_dev_uncolored.py
	@echo
	@echo "========================================================================================"
	@echo "Dev output (no logger name column)"
	pipenv run python examples/ex_dev_nologgername.py
	@echo
	@echo "========================================================================================"
	@echo "Dev+GELF output"
	pipenv run python examples/ex_gelf.py
	@echo
	@echo "========================================================================================"
	@echo "JSON output"
	pipenv run python examples/ex_json.py


# ==================================================================================================
# Documentation targets
# ==================================================================================================

DOCS_EXCLUSION=$(foreach m, $(MODULES), $m/tests)

docs: clean-docs sdist docs-generate-apidoc docs-run-sphinx

docs-generate-apidoc:
	pipenv run sphinx-apidoc \
		--force \
		--separate \
		--module-first \
		--doc-project "API Reference" \
		-o docs/source/reference \
		$(PACKAGE_NAME) \
			$(DOCS_EXCLUSION)

docs-run-sphinx:
	pipenv run make -C docs/ html

clean-docs:
	rm -rf docs/_build docs/source/reference/*.rst

docs-open:
	xdg-open docs/_build/html/index.html

upload:
	nestor upload -am

upload-ready:
	nestor upload -ram -w @swlabs/nestor/reviewers


sctu: sct upload

sctur: sct upload-ready

# aliases to gracefully handle typos on poor dev's terminal
check: checks
devel: dev
develop: dev
dist: dists
install: install-system
pypi: pypi-publish
styles: style
wheel: wheels
