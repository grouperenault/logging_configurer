# logging_configurer

[![pipeline status](https://gitlab.com/grouperenault/logging_configurer/badges/master/pipeline.svg)](https://gitlab.com/grouperenault/logging_configurer/commits/master)
[![coverage report](https://gitlab.com/grouperenault/logging_configurer/badges/master/coverage.svg)](http://grouperenault.gitlab.iologging_configurer/coverage/index.html)

## Features

Opinionated configuration of Python logging for Renault Software Labs.

This is what we use internally to configure our structured log in Kubernetes environments.
Contains lots of tweaks common to our apps for structure logging especially for gelf logging.
We find GELF more scalable as it avoids us to put a side car to all our containers. YMMV.

This is a dependency for our other open-source projects until we find a better and more generic solution.

* [Documentation](https://grouperenault.gitlab.io/logging_configurer/docs/index.html)
* [Coverage Report](https://grouperenault.gitlab.io/logging_configurer/coverage/index.html)

## Usage (shared development)

Install in your python project with pipenv:

```bash
pipenv install logging_configurer
```

To get colorful logs during development, install `colorama` as well:

```bash
pipenv install --dev colorama
```

Call it at the beggining of your application:

```python
from logging_configurer import LoggingConfigurer
from logging_configurer import get_logger

log = get_logger()
def main():
    logger_conf = LoggingConfigurer()
    logger_conf.set_level(True)  # equivalent to logger_conf.set_level(logging.DEBUG)
    logger_conf.set_output_type("dev")
    logger_conf.set_gelf_config("127.0.0.1", 1234)
    logger_conf.set_application_info(name="myapp", flavor="prod", version="1.0.2")
    logger_conf.hide_unwanted_string("mypassword")
    logger_conf.set_uuid()
    logger_conf.apply()
    log.debug("Logger configured")
```

You can also have it read environment variables automatically:

```python
# * MYAPP_DEBUG=1  # equivalent to MYAPP_LOG_LEVEL=DEBUG
# * MYAPP_OUTPUT_TYPE=dev
# * MYAPP_GELF_HOST=127.0.0.1
# * MYAPP_GELF_PORT=1234
# * MYAPP_GELF_ENABLED= # optional if MYAPP_GELF_HOST is set
# * MYAPP_GELF_LOG_LEVEL=DEBUG
def main():
    logger_conf = LoggingConfigurer(env_prefix="MYAPP_")
    logger_conf.apply()
```

List of valid environment variable (`MYAPP_` being the variable part (can be empty string)):

* `MYAPP_DEBUG=1`: equivalent of `MYAPP_LOG_LEVEL=DEBUG`. Set the log level for the standard output.
  Available values:

  * `0`: DEBUG log level
  * `1`: INFO log level
  * `DEBUG`: DEBUG log level
  * `INFO`: INFO log level
  * `WARN`: WARN/WARNING log level
  * `ERROR`: ERROR log level
  * `CRITICAL`: CRITICAL log level

* `MYAPP_OUTPUT_TYPE=dev`: standard output type.
  Available values:

  * `none`: not standard output
  * `plain`: textual message
  * `dev`: colorful developer-oriented output
  * `json`: output each log using a single line JSON chunk.

* `MYAPP_GELF_HOST=127.0.0.1`: if set, enable output to a GELF server and set its hostname (without
  the port)
* `MYAPP_GELF_PORT=1234`: port to use for the GELF output
* `MYAPP_GELF_SERVER=grayserver.example.com:1234`: shortcut instead of GELF_HOST and GELF_PORT
* `MYAPP_GELF_ENABLED`: optional if `MYAPP_GELF_HOST` is set
* `MYAPP_GELF_LOG_LEVEL=DEBUG`: set the GELF output log level. See `MYAPP_DEBUG` for available
  values
* `MYAPP_GELF_FLAT_EXTRA=1`: if set, flatten the output of all extra context in a single multiline
  node named `extra.flat`. Please note another key is created, `extra.values`, where only
  string, integers and float are stored (so all dict, jsonable, set, list are simply filtered out).
* `MYAPP_GELF_FLAT_EXTRA_SEP="\n"`: change the character sequence to separate each context value.
  By default it is a new line.

## context of log

When using Gelf, some additional data can be bring with the log:

* To add application info:
  [LoggingConfigurer().set_application_info](reference/logging_configurer.html#logging_configurer.LoggingConfigurer.set_application_info)
* To add a global uuid: [LoggingConfigurer().set_uuid()](reference/logging_configurer.html#logging_configurer.LoggingConfigurer.set_uuid)
* To add a global dictionary: [logging_configurer.add_context_to_gelf_output()](reference/logging_configurer.html#logging_configurer.add_context_to_gelf_output)
* To add a task context dictionary (based on contextvars):
  [logging_configurer.context.update()](reference/logging_configurer.context.html#logging_configurer.context.update).
  Please note this is not available by default for Python < 3.7.
  A backport exists `pip install contextvars` for Python 3.6 only.

## Note for external python package

We might want to use structlog but not necessarily adds the dependency on
``LoggingConfigurer`` in your package. If so, please ensure to use the ``BoundLogger`` wrapper in
your logger:

```python

from structlog import get_logger
from structlog.stdlib import BoundLogger

log = get_logger(wrapper_class=BoundLogger)
```

This will ensure all logs are properly sent to standard logging facility, even in unit tests.
This only impacts if you use lazy argument evaluation in your log, as in:

```python
log = get_logger(wrapper_class=BoundLogger)
log.debug("value=%s", value)
```

But most of the time, you would use structlog power:

```python
log.debug("event description", value=value, otherval=otherval)
```

## Note for GELF output

Do not use the following keywords in your message, as they might conflict with Python's logging
keys names:

* ``_record``
* ``event``
* ``extra``
* ``level``
* ``logger``
* ``msg``
* ``timestamp``

You can also define a custom dictionary that will be sent to **all** logs automatically, only to
the GELF and JSON outputs, using the following function:

```python
from logging_configurer import add_context_to_gelf_output

add_context_to_gelf_output({
    "a_general_stuff": "value1",
    "another_general_stuff": "value2",
})
```

## Note for maintainers

Do **not** forget to update `requirements.txt` if you change dependency with:

```bash
make requirements
```

PBR is not able yet to read the `Pipfile`. So in order to the dependency
of this project to be installed, you **need** to update `requirements.txt` each time it changes.
