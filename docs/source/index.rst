.. Logging Configurer documentation master file, created by
   sphinx-quickstart on Wed Jan 10 10:32:45 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Logging Configurer' documentation!
=============================================

.. toctree::
    :maxdepth: 2
    :caption: Table of Contents
    :glob:

    readme
    changelog
    reference/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
