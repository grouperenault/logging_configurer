# -*- coding: utf-8 -*-

# Standard Library
import logging

# Third Party Libraries
import urllib3

# Logging Configurer Modules
from logging_configurer import add_context_to_gelf_output
from logging_configurer import add_unwanted_string_in_structlog
from logging_configurer import get_logger

log_st = get_logger()
log_lg = logging.getLogger(__name__)

# pylint: disable=too-many-statements


def logging_example():
    unwanted = "UNWANTED STRING IN LOG"
    add_unwanted_string_in_structlog(unwanted)
    log_st.critical("[structlog] START", v="1", a=unwanted)
    log_lg.critical("[logging  ] START %s", unwanted)

    log_st.debug("[structlog] debug")
    log_lg.debug("[logging  ] debug")

    log_st.info("[structlog] info")
    log_lg.info("[logging  ] info")

    log_st.warning("[structlog] warning")
    log_lg.warning("[logging  ] warning")

    log_st.error("[structlog] error")
    log_lg.error("[logging  ] error")

    log_st.critical("[structlog] critical")
    log_lg.critical("[logging  ] critical")

    try:
        raise ValueError("an exception")
    except ValueError:
        log_st.exception("[structlog] exception")
        log_lg.exception("[logging  ] exception")

    log_st.info("[structlog] lazy param val=%s", "123")
    log_lg.info("[logging  ] lazy param val=%s", "123")

    log_st.info("[structlog] parameterized log (str)", val="123")
    log_lg.info("[logging  ] parameterized log (str)", extra={"val": "123"})

    log_st.info("[structlog] parameterized log (list)", val=["a", "b", "c"])
    log_lg.info("[logging  ] parameterized log (list)", extra={"val": ["a", "b", "c"]})

    log_st.info(
        "[structlog] parameterized log (dict) with lazy a=%s",
        "123",
        val={"a": "123", "b": 456, "c": 1.0},
    )
    log_lg.info(
        "[logging  ] parameterized log (dict) with lazy a=%s",
        "123",
        extra={"val": {"a": "123", "b": 456, "c": 1.0}},
    )

    log_st.info(
        "[structlog] parameterized log (dict with unserializable)",
        val={"a": "123", "b": 456, "c": ValueError},
    )
    log_lg.info(
        "logging logger: parameterized log (dict with unserializable)",
        extra={"val": {"a": "123", "b": 456, "c": ValueError}},
    )

    log_st.info("[structlog] parameterized log (val unserializable)", val=ValueError)
    log_lg.info(
        "[logging  ] parameterized log (val unserializable)", extra={"val": ValueError}
    )

    # pylint: disable=logging-format-interpolation
    log_st.warning("[structlog] unwanted string in event: {}".format(unwanted))
    log_lg.warning("[logging  ] unwanted string in event: {}".format(unwanted))
    # pylint: enable=logging-format-interpolation

    log_st.warning("[structlog] unwanted string in lazy val: %s", unwanted)
    log_lg.warning("[logging  ] unwanted string in lazy val: %s", unwanted)

    log_st.warning(
        "[structlog] unwanted string in lazy list: %s",
        [1, 2, "prefix " + unwanted + ", suffix"],
    )
    log_lg.warning(
        "[logging  ] unwanted string in lazy list: %s",
        [1, 2, "prefix " + unwanted + ", suffix"],
    )

    log_st.warning("[structlog] unwanted string in param", unwanted_str=unwanted)
    log_lg.warning(
        "[logging  ] unwanted string in param", extra={"unwanted_str": unwanted}
    )

    log_st.warning(
        "[structlog] unwanted string in param 2", unwanted_dict={"a": unwanted}
    )
    log_lg.warning(
        "[logging  ] unwanted string in param 2",
        extra={"unwanted_dict": {"a": unwanted}},
    )

    try:
        raise ValueError("an exception with unwanted string: " + unwanted)
    except ValueError:
        log_st.exception("[structlog] unwanted string in exception")
        log_lg.exception("[logging  ] unwanted string in exception")

    log_st.warning(
        "[structlog] param and extra", val="123", extra={"val_in_extra": "value"}
    )
    log_st.warning(
        "[logging  ] param and extra", extra={"val": "123", "val_in_extra": "value"}
    )

    add_context_to_gelf_output(
        {"a_general_stuff": "value1", "another_general_stuff": "value2",}
    )

    log_st.warning("[structlog] warning where context log should be inserted")
    log_lg.warning("[logging  ] warning where context log should be inserted")

    log_st.error("[structlog] error where context log should be inserted")
    log_lg.error("[logging  ] error where context log should be inserted")

    http = urllib3.PoolManager()
    http.request("GET", "httpbin.org/")
