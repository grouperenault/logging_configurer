#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Show log using "dev" profile (colorful) with gelf output.

Run me with:

    pipenv run ./examples/ex_gelf.py

"""
# Standard Library
import logging
import sys
from pathlib import Path
from uuid import uuid4

# Logging Configurer Modules
from _common import logging_example
from logging_configurer import LoggingConfigurer
from logging_configurer import get_logger

# noqa: E402
# pylint: disable=duplicate-code, wrong-import-position

# Injecting 'examples/_common' syspath
sys.path.insert(0, str(Path(__file__).parent.resolve()))  # pylint: disable=no-member

log_st = get_logger()
log_lg = logging.getLogger(__name__)


def main():
    print("This example should:")
    print(" - log to stdout in WARN level")
    print(" - use colors if colorama is installed")
    print(" - log to GELF in INFO level")
    print(" - add a UUID field in GELF output")

    logger_conf = LoggingConfigurer()
    logger_conf.set_level(logging.WARN)
    logger_conf.set_application_info(
        name="logging_configurer_gelf_demo",
        flavor="test",
        version="1.0.0",
        fingerprint=uuid4(),
    )
    uuid = logger_conf.set_uuid()

    print(
        "These logs are sent to an logstash node that route everything to elasticseach and "
        "can be seeing ultimately on Kibana"
    )
    print("Here are some information about the job:")
    print("  - Job UUID: {}".format(uuid))
    print(
        "  - Kibana URL: http://kibops.kube.tls.renault.fr/app/kibana#/discover?"
        "_g=(refreshInterval:(display:Off,pause:!f,value:0),"
        "time:(from:now%2Fd,mode:quick,to:now%2Fd))"
        "&_a=(columns:!(levelname,syslog_message,extra.flat),"
        "index:'0a90c260-1d6d-11e8-97e0-99154df17326',"
        "interval:auto,"
        "query:(language:lucene,query:'uuid:%22{}%22'),sort:!('@timestamp',asc))".format(
            uuid
        )
    )

    logger_conf.set_output_type("dev")
    logger_conf.set_gelf_config(
        "gelfserver.example.com", 9313, level=logging.INFO, flat_extra=True
    )
    logger_conf.apply()
    logging_example()


if __name__ == "__main__":
    main()
