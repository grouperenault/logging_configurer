#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Show log using "dev" profile (colorful).

Run me with:

    pipenv run ./examples/ex_dev.py

"""

# Standard Library
import logging
import sys
from pathlib import Path

# Logging Configurer Modules
from _common import logging_example
from logging_configurer import LoggingConfigurer
from logging_configurer import get_logger

# noqa: E402
# pylint: disable=duplicate-code, wrong-import-position

# Injecting 'examples/_common' syspath
sys.path.insert(0, str(Path(__file__).parent.resolve()))  # pylint: disable=no-member

log_st = get_logger()
log_lg = logging.getLogger(__name__)


def main():
    print("This example should output each logs without any context, timestamp, etc")

    logger_conf = LoggingConfigurer()
    logger_conf.set_level("DEBUG")
    logger_conf.set_output_type("plain")
    logger_conf.apply()
    logging_example()


if __name__ == "__main__":
    main()
