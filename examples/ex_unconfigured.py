#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Standard Library
import logging
import sys
from pathlib import Path

# Logging Configurer Modules
from _common import logging_example
from logging_configurer import get_logger

# noqa: E402
# pylint: disable=duplicate-code, wrong-import-position

# Injecting 'examples/_common' syspath
sys.path.insert(0, str(Path(__file__).parent.resolve()))  # pylint: disable=no-member

log_st = get_logger()
log_lg = logging.getLogger(__name__)


def main():
    print(
        "This example shows how logs behave **without** logging configurer, where logs from "
        "structlog.get_logger are mixed with logging.getLogger"
    )
    logging_example()


if __name__ == "__main__":
    main()
