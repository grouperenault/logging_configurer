# -*- coding: utf-8 -*-
"""
Configure logger according to your environment

Usage
-----

Manualy define settings:

.. code-block:: python

    from logging_configurer import LoggingConfigurer

    def main():
        logger_conf = LoggingConfigurer()
        logger_conf.set_level(False)
        logger_conf.set_output_type("dev")
        logger_conf.set_gelf_config("127.0.0.1", 1324)
        logger_conf.apply()

Inherit from environment variables:

.. code-block:: python

    from logging_configurer import LoggingConfigurer

    def main():
        logger_conf = LoggingConfigurer(env_prefix="MY_APP_")
        logger_conf.apply()

Hide sentitive password and insert UUID in json/gelf outputs:

.. code-block:: python

    from logging_configurer import LoggingConfigurer

    def main():
        logger_conf = LoggingConfigurer(env_prefix="MY_APP_")
        LoggingConfigurer.hide_unwanted_string("mypassword")
        LoggingConfigurer.set_uuid("1324245")
        logger_conf.apply()

Environments
------------

- `plain`: logs are just outputed on the stdout as any normal application.
    Usage: docker output
- `dev`: colorful, developer-friendly output.
    Usage: development environment
- `json`: logs are output on stdout, one json chunk per line
- `none`: no logs are output on stdout

In addition, you can enable Gelf output to also route logs to GELF/Logstash:
- `gelf`: logs are not outputed on stdout but routed to a GELF UDP host/port

Architecture
------------

Two loggers can be used:

- classic `logging.getLogger`: You may not use it directly and prefer the `structlog` logger,
    but you probably use third party libraries that use logging.
- advanced `structlog.get_logger`: using it allows to use advanced feature such as key
    items

Both can be used as the same time, ultimately, everything goes through `logging` handler
before being sent to the right formatter and output handler.

dev/plain/json::

    structlog.logger => logging handler => structlog processor formatter => logging output
    logger.logger    => logging handler => structlog processor formatter => logging output

GELF is a special case in that logging loggers cannot directly to Gelf encoder::

    structlog logger => event_to_gelf => GelfHandler
    logging logger   => logging handler => GelfHandler (custom encoder impl)

Processor formatter allow to add a level of security to automatically hide sensitive
informations such as password in ALL logs, structlog's AND logging's loggers.

Features
--------

- **sensitive information hiding**: simply register your unwanted strings with
    `logging_configurer.add_unwanted_string_in_structlog`
- **auto insert UUID**: to insert a unique identifier in every log (json/gelf output only),
    simple call `LoggingConfigurer.set_uuid()` api. Beware LoggingConfigurer.set_uuid does NOT
    support any kind of concurrency, there cannot have 2 tasks with different ID at the same
    time, especially in the case of coroutine framework with twisted/asyncio, set_uuid cannot
    be trusted.
"""

# Logging Configurer Modules
from logging_configurer import context
from logging_configurer._logging_configurer import LoggingConfigurer
from logging_configurer._logging_configurer import add_unwanted_string_in_structlog
from logging_configurer._structlog_logger import get_logger
from logging_configurer.processors import get_uuid
from logging_configurer.renderers import add_context_to_gelf_output

__all__ = [
    "add_context_to_gelf_output",
    "add_unwanted_string_in_structlog",
    "get_logger",
    "get_uuid",
    "context",
    "LoggingConfigurer",
]
