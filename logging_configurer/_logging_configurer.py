# -*- coding: utf-8 -*-

# Standard Library
import logging
import logging.config
import os
import socket
import sys
import time
from uuid import uuid4

# Third Party Libraries
import structlog

# Logging Configurer Modules
from logging_configurer.customized_gelf_handler import MyGELFHandler
from logging_configurer.processors import g_uuid
from logging_configurer.processors import processor_insert_application_data
from logging_configurer.renderers import ConsoleRenderer
from logging_configurer.renderers import PlainRenderer
from logging_configurer.renderers import add_unwanted_string_in_structlog
from logging_configurer.renderers import hide_unwanted_strings
from logging_configurer.renderers import reset_unwanted_strings

# pylint: disable=too-many-locals,too-many-branches,too-few-public-methods

log = structlog.get_logger("logging_configurer")

ALLOWED_OUTPUT_TYPES = ["plain", "dev", "json", "none"]


class LoggingConfigurer(object):
    """Automatically configure the logging subsystem

    Available output modes:

    - ``plain``: display only event message
    - ``dev``: display logger name, event message, with timestamp and extra values in
      nice multilines.
      Options:
      - colorful (if colorama package is set)
      - disable logger name (see set_dev_option function)
    - ``json``: output the event in a 1 line json
    - ``none``: no output

    Optionally enable GELF with `set_gelf_config`.

    To have colorful output, install ``colorama`` python module in your environment.

    Example:

    .. code-block:: python

        from logging_configurer import LoggingConfigurer
        from logging_configurer import get_logger

        log = get_logger()
        def main():
            logger_conf = LoggingConfigurer()
            logger_conf.set_level(True)  # equivalent to logger_conf.set_level(logging.DEBUG)
            logger_conf.set_output_type("dev")
            logger_conf.set_gelf_config("127.0.0.1", 1234, index='logstash')
            logger_conf.hide_unwanted_string("mypassword")
            logger_conf.set_uuid("1324245")
            logger_conf.apply()
            log.debug("Logger configured")
    """

    _level = logging.INFO
    _output_type = "plain"
    _gelf_handler = None
    _gelf_enabled = False
    _gelf_host = None
    _gelf_port = None
    _gelf_level = logging.INFO
    _colors = True
    _gelf_flat_extra = False
    _gelf_flat_extra_sep = "\n"
    _dev_show_loggername = True
    _index = None

    def __init__(self, env_prefix=None):
        self._env_prefix = env_prefix
        self._human_timestamper = structlog.processors.TimeStamper(
            fmt="%Y-%m-%d %H:%M:%S.%f"
        )
        self._iso_timestamper = structlog.processors.TimeStamper(fmt="iso")
        self._pre_chain = [
            # Add the log level and a timestamp to the event_dict if the log entry
            # is not from structlog.
            structlog.stdlib.add_log_level,
            structlog.stdlib.add_logger_name,
            structlog.processors.format_exc_info,
            structlog.stdlib.PositionalArgumentsFormatter(),
            self._processor_hide_unwanted_strings,
            self._processor_hide_password,
        ]

    @staticmethod
    def hide_unwanted_string(unwanted_string):
        add_unwanted_string_in_structlog(unwanted_string)

    @staticmethod
    def reset_unwanted_strings():
        reset_unwanted_strings()

    def set_level(self, level):
        self._level = self._translate_level(level)

    def set_debug(self, debug: bool = True):
        """
        Enables the debug level.

        Optional arguments:
            debug (bool, optional): Defaults to True. If set to False it log level is set to INFO.
        """
        self.set_level(debug)

    def set_output_type(self, output_type: str):
        """set the output type

        Args:
            output_type (str): One of the supported output types.
        """
        assert output_type in ALLOWED_OUTPUT_TYPES

        self._output_type = output_type

    def disable_colors(self):
        """Disable color output in stdout
        """
        self._colors = False

    def set_dev_option(self, show_loggername=True):
        """Set option for the "dev" output type.

        Arguments
          show_loggername: show the logger name column
        """
        self._dev_show_loggername = show_loggername

    def set_application_info(self, name, flavor=None, version=None, fingerprint=None):
        """Set application info that will be set to every JSON log (json output or GELF)

        Arguments:
            name (str): name of the application. Set the "application_name" field in the json.
            flavor (str, optional): flavor of the application. Set the "application_flavor" field
                                    in the json.
            version (str, optional): version of the application. Set the "application_version"
                                     field in the json.
            fingerprint (str, optional): fingerprint of the current build of the application.
                                         This is typically a "UUID" generated at the beginning of
                                         your build.
                                         Set the "application_fingerprint" field in the json.
        """
        from logging_configurer.processors import g_application_info

        g_application_info["name"] = str(name) if name else None
        g_application_info["flavor"] = str(flavor) if flavor else None
        g_application_info["version"] = str(version) if version else None
        g_application_info["fingerprint"] = str(fingerprint) if fingerprint else None

    def set_gelf_config(
        self,
        host,
        port,
        level=logging.DEBUG,
        flat_extra=False,
        flat_extra_sep="\n",
        index=None,
    ):
        """Enable the GELF output

        Args:
            host (str): IP or URL (without port) to the GELF server
            port (str): UDP port to send GELF data to
            level (logging level, optional): Defaults to logging.DEBUG. Log level
            flat_extra (bool, optional): Defaults to False. Do we flatten all 'extra' keys in a
                                         multiline string?
            flat_extra_sep (bool, str): Defaults to "\\n". Separator string between each element in
                                        flattened "extra" string
            index (str, optional): Defaults to None. Sets the index to use in ElasticSearch.
        """
        self._gelf_host = host
        self._gelf_port = port
        self._gelf_level = level
        self._gelf_flat_extra = flat_extra
        self._gelf_flat_extra_sep = flat_extra_sep
        self._index = index

    def wait_for_gelf(self, timeout=120):
        """
        Wait for gelp instance to be ready

        In some configuration (e.g.: k8s pods) we have to wait for gelp boot
        up before starting logging.
        This helper method is doing the job, return false after timeout if
        gelf is not ready.

        :param timeout: time (in seconds) to wait for gelf
        :return: True if ok, False if gelf is not responding
        """
        if not self._gelf_enabled:
            # Gelf not enabled, nothing to wait
            return True

        for _ in range(0, timeout):
            try:
                if socket.gethostbyname(self._gelf_host):
                    return True
            except socket.gaierror:
                time.sleep(1)
                # logger is not ready yet, so use print
                print("gelf host {} not found, retrying".format(self._gelf_host))

        return False

    def _set_from_env(self):
        self._level = self._translate_level(
            os.environ.get(
                self._env_prefix + "DEBUG",
                os.environ.get(self._env_prefix + "LOG_LEVEL", self._level),
            )
        )
        self._output_type = os.environ.get(
            self._env_prefix + "OUTPUT_TYPE", self._output_type
        )
        self._colors = not os.environ.get(
            self._env_prefix + "DISABLE_COLORS", not self._colors
        )
        self._dev_show_loggername = os.environ.get(
            self._env_prefix + "DEV_SHOW_LOGGERNAME", self._dev_show_loggername
        )
        gelf_host = os.environ.get(self._env_prefix + "GELF_HOST")
        gelf_port = os.environ.get(self._env_prefix + "GELF_PORT")
        gelf_server = os.environ.get(self._env_prefix + "GELF_SERVER")
        if gelf_server is not None:
            gelf_host, gelf_port = gelf_server.split(":")
        gelf_level = self._translate_level(
            os.environ.get(self._env_prefix + "GELF_LOG_LEVEL", self._gelf_level)
        )
        gelf_flat_extra = os.environ.get(self._env_prefix + "GELF_FLAT_EXTRA", "0")
        gelf_flat_extra = bool(
            gelf_flat_extra == "1"
            or gelf_flat_extra == 1
            or gelf_flat_extra.lower() == "true"
        )
        gelf_flat_extra_sep = os.environ.get(
            self._env_prefix + "GELF_FLAT_EXTRA_SEP", "\n"
        )
        gelf_index = os.environ.get(self._env_prefix + "GELF_INDEX", None)

        self._gelf_enabled = os.environ.get(
            self._env_prefix + "GELF_ENABLED", gelf_host is not None
        )
        if self._gelf_enabled:
            self.set_gelf_config(
                gelf_host,
                gelf_port,
                gelf_level,
                flat_extra=gelf_flat_extra,
                flat_extra_sep=gelf_flat_extra_sep,
                index=gelf_index,
            )

    @staticmethod
    def _translate_level(level):
        if level == "1" or level == 1 or level is True:
            return logging.DEBUG
        if level == "0" or level == 0 or level is False:
            return logging.INFO
        if isinstance(level, str):
            # pylint: disable=W0212
            return logging._nameToLevel.get(level.strip().upper(), logging.NOTSET)
            # pylint: enable=W0212
        return level

    @staticmethod
    def set_uuid(uuid=None, task_name=None):
        """Insert a "uuid" field in every log, to link them together.

        Json and graylog outputs only.

        Arguments:
            uuid: UUID to use. If set to none, a random one is generated
            task_name: name of the task, optional

        Returns:
            The UUID used during this session.
        """
        if uuid is None:
            uuid = uuid4()
        global g_uuid  # pylint: disable=W0603
        g_uuid["uuid"] = str(uuid)
        g_uuid["task_name"] = task_name
        return g_uuid["uuid"]

    def apply(self):
        """Apply the current configuration (all previous ``set_*`` methods)

        Raises:
            NotImplementedError: on invalid output type
        """

        if self._env_prefix is not None:
            self._set_from_env()

        if self._output_type.lower() == "gelf":
            # Ensure backward compatibility
            self._output_type = "none"

        if self._output_type.lower() == "dev":
            self._apply_dev()
        elif self._output_type.lower() == "json":
            self._apply_json()
        elif self._output_type.lower() == "plain":
            self._apply_plain()
        elif self._output_type.lower() == "none":
            # Nothing to do
            pass
        else:
            raise NotImplementedError(
                "Not supported output type: {}. Available: {}".format(
                    self._output_type, ALLOWED_OUTPUT_TYPES
                )
            )

        if self._gelf_host:
            self._apply_gelf()
        if g_uuid["uuid"] and self._output_type in ["graylog", "json"]:
            log.info("Defined UUID to {}".format(g_uuid["uuid"]))

    def _setup_logging(self, formatter_name="plain"):
        assert formatter_name in ALLOWED_OUTPUT_TYPES

        cfg = {
            "version": 1,
            "disable_existing_loggers": False,
            "formatters": {},
            "handlers": {},
            "loggers": {},
        }
        # pylint: disable=protected-access
        if formatter_name in ["plain", "dev", "json"]:
            cfg["handlers"] = {
                "default": {
                    "level": logging._levelToName.get(self._level, "NOTSET"),
                    "class": "logging.StreamHandler",
                    "stream": sys.stdout,
                    "formatter": formatter_name,
                },
            }
            cfg["loggers"] = {
                "": {"handlers": ["default"], "level": "DEBUG", "propagate": True,},
            }
            if formatter_name == "plain":
                cfg["formatters"]["plain"] = {
                    "()": structlog.stdlib.ProcessorFormatter,
                    "processor": PlainRenderer(),
                    "foreign_pre_chain": self._pre_chain,
                }
            if formatter_name == "dev":
                cfg["formatters"]["dev"] = {
                    "()": structlog.stdlib.ProcessorFormatter,
                    "processor": ConsoleRenderer(
                        colors=self._colors,
                        pad_loggername=35 if self._dev_show_loggername else 0,
                    ),
                    "foreign_pre_chain": [self._human_timestamper] + self._pre_chain,
                }
            if formatter_name == "json":
                cfg["formatters"]["json"] = {
                    "()": structlog.stdlib.ProcessorFormatter,
                    "processor": structlog.processors.JSONRenderer(),
                    "foreign_pre_chain": [self._iso_timestamper]
                    + self._pre_chain
                    + [processor_insert_application_data],
                }
        # pylint: enable=protected-access
        logging.config.dictConfig(cfg)

    def _apply_dev(self):
        self._setup_logging("dev")
        structlog.configure(
            processors=[
                structlog.stdlib.add_logger_name,
                structlog.stdlib.add_log_level,
                structlog.stdlib.PositionalArgumentsFormatter(),
                self._human_timestamper,
                structlog.processors.StackInfoRenderer(),
                structlog.processors.format_exc_info,
                self._processor_hide_unwanted_strings,
                self._processor_hide_password,
                structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
            ],
            context_class=dict,
            logger_factory=structlog.stdlib.LoggerFactory(),
            wrapper_class=structlog.stdlib.BoundLogger,
            cache_logger_on_first_use=True,
        )

    def _apply_json(self):
        self._setup_logging("json")
        structlog.configure(
            processors=[
                self._iso_timestamper,
                structlog.stdlib.add_logger_name,
                structlog.stdlib.add_log_level,
                structlog.stdlib.PositionalArgumentsFormatter(),
                structlog.processors.StackInfoRenderer(),
                structlog.processors.format_exc_info,
                self._processor_hide_unwanted_strings,
                self._processor_hide_password,
                processor_insert_application_data,
                structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
            ],
            context_class=dict,
            logger_factory=structlog.stdlib.LoggerFactory(),
            wrapper_class=structlog.stdlib.BoundLogger,
            cache_logger_on_first_use=True,
        )

    def _apply_plain(self):
        self._setup_logging("plain")
        structlog.configure(
            processors=[
                structlog.stdlib.add_log_level,
                structlog.stdlib.PositionalArgumentsFormatter(),
                self._human_timestamper,
                structlog.processors.StackInfoRenderer(),
                structlog.processors.format_exc_info,
                self._processor_hide_unwanted_strings,
                self._processor_hide_password,
                structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
            ],
            context_class=dict,
            logger_factory=structlog.stdlib.LoggerFactory(),
            wrapper_class=structlog.stdlib.BoundLogger,
            cache_logger_on_first_use=True,
        )

    def _apply_gelf(self):
        # Gelf configuration is more complex.
        # StreamHandler does not support formatter, so we cannot use
        #   structlog.stdlib.ProcessorFormatter
        # to route directly bare logging to the GELFEncoder.
        # So we use two different pipeline
        # structlog logger => GELFEncoder => MyGelfHandler
        # logging logger   => MyGelfHandler (custom encoder impl)
        assert self._gelf_host
        assert self._gelf_port
        std_logger = logging.getLogger("")
        std_logger.setLevel(self._gelf_level)
        gelf_handler = MyGELFHandler(
            host=self._gelf_host,
            port=int(self._gelf_port),
            extra_fields=True,
            foreign_pre_chain=[
                structlog.processors.TimeStamper(fmt="iso", utc=True),
                # Add the log level and a timestamp to the event_dict if the log entry
                # is not from structlog.
                structlog.stdlib.add_log_level,
                structlog.stdlib.add_logger_name,
                self._processor_hide_unwanted_strings,
                self._processor_hide_password,
                processor_insert_application_data,
                structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
            ],
            index=self._index,
        )
        if self._gelf_flat_extra:
            gelf_handler.set_flat_extra(sep=self._gelf_flat_extra_sep)
        std_logger.addHandler(gelf_handler)

    def _processor_hide_unwanted_strings(self, _1, _2, event_dict):
        """"Hide unwanted string in the logs

        Use to hard search+replace your password before they are stored in logs
        """
        return hide_unwanted_strings(event_dict)

    def _processor_hide_password(self, _, __, event_dict):
        """
        Simpler password hider

        It just censor keys named 'password'. For more hardcode password censorship, use
        _processor_hide_unwanted_strings.
        """

        pw = event_dict.get("password")
        if pw:
            event_dict["password"] = "****"
        return event_dict
