# -*- coding: utf-8 -*-

# Third Party Libraries
from structlog import wrap_logger as _structlog_wrap_logger
from structlog.stdlib import BoundLogger as _structlog_BoundLogger


def get_logger(*args, **initial_values):
    """Logging friendly default logger
    """

    wrapper_class = _structlog_BoundLogger
    return _structlog_wrap_logger(
        None, logger_factory_args=args, wrapper_class=wrapper_class, **initial_values
    )
