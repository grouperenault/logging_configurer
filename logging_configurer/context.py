# -*- coding: utf-8 -*-

# Standard Library
from typing import Optional  # pylint: disable=unused-import

try:
    from contextvars import ContextVar
    from contextvars import Token  # pylint: disable=unused-import
except ImportError:
    # contextvars may not be available on Python 3.6 with aio support
    ContextVar = None
    Token = None

g_ctxvar = None


def _renew():
    """Permit to create a new instance of context.

    this is mainly for test purpose
    """
    global g_ctxvar
    if ContextVar is None:
        return
    g_ctxvar = ContextVar("logging_configurer_context", default={})


def is_initialized() -> bool:
    """Return true if initialized."""
    return g_ctxvar is not None


def get_copy():
    """Return the current value of ContextVar."""
    if not is_initialized():
        _renew()
    if g_ctxvar is None:
        return None
    return g_ctxvar.get().copy()


def update(dic: dict) -> "Optional[Token]":
    """Update_context amend the current context with dic.

    Returns the contextvars token (to pass to :func:`reset()` to rollback the
    update).

    Note: not available for Python 3.5. Python 3.6 requires an additional
    backport install: ``pip install contextvars``.
    """
    if not isinstance(dic, dict):
        raise ValueError
    # To avoid concurrent access to the same ref ctx must be a copy of
    # context value
    ctx = get_copy()
    if ctx is None:
        return None
    ctx.update(dic)
    return g_ctxvar.set(ctx)


def reset(token: "Token"):
    """Reset the contextvar."""
    if ContextVar is None:
        return
    if not is_initialized():
        _renew()
        return
    g_ctxvar.reset(token)
