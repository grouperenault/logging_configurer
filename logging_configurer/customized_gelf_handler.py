# -*- coding: utf-8 -*-

# Standard Library
import logging
import logging.config
import numbers
import os
import socket
import sys
import zlib

# Third Party Libraries
import graypy.handler

# Logging Configurer Modules
from logging_configurer.processors import cleanup_event
from logging_configurer.processors import processor_insert_application_data
from logging_configurer.renderers import g_context_dict
from logging_configurer.renderers import hide_unwanted_strings

# pylint: disable=too-many-locals,too-many-branches,too-few-public-methods,too-many-statements

# cache the process_name.
if sys.argv:
    g_process_name = sys.argv[0].rsplit(os.path.sep, 1)[-1]
else:
    g_process_name = "python"


def set_process_name(pn):
    """Configure entrypoint to set the process name of the logger."""
    global g_process_name
    g_process_name = pn


class MyGELFHandler(graypy.handler.GELFHandler):
    def __init__(self, foreign_pre_chain, *args, index=None, **kwargs):
        self.flat_extra = False
        self.flat_extra_sep = ""
        self.index = index
        super(MyGELFHandler, self).__init__(*args, **kwargs)
        if foreign_pre_chain:
            self.foreign_pre_chain = foreign_pre_chain
        else:
            self.foreign_pre_chain = []

    def set_flat_extra(self, sep="\n"):
        self.flat_extra = True
        self.flat_extra_sep = sep

    def makePickle(self, record):
        message_dict = self.make_message_dict(
            record,
            self.debugging_fields,
            self.extra_fields,
            self.fqdn,
            self.localname,
            self.level_names,
            self.facility,
            self.index,
        )
        packed = graypy.handler.message_to_pickle(message_dict)
        frame = zlib.compress(packed) if self.compress else packed
        return frame

    def make_message_dict(  # noqa: C901
        self,
        record,
        debugging_fields: bool,
        extra_fields: bool,
        fqdn,
        localname,
        level_names,
        facility=None,
        index=None,
    ):

        syslog_message = record.getMessage()
        full_message = record.msg

        if not isinstance(record.msg, dict):
            # Reinject prechain in logs that does not come from structlog.get_logger
            ed = {"event": record.getMessage(), "_record": record}
            # dirty replacement of ProcessorFormatter.format() that does not work well with GELF
            for proc in self.foreign_pre_chain:
                meth_name = record.levelname.lower()
                ed = proc(None, meth_name, ed)
            if isinstance(ed, tuple):
                # ed is a tuple, so the result of wrap_for_formatter
                event_dict_tupl, _extra_dict = ed
                event_dict = event_dict_tupl[0]
                # extra = extra_dict.get("extra")
                # logger = extra.get("_logger")
                # name = extra.get("_name")
                ed = event_dict.copy()  # pylint: disable=no-member
                # pylint: disable=invalid-sequence-index
                ed.update({"msg": event_dict["event"], "_record": record})
                # pylint: enable=invalid-sequence-index
                full_message = ed["event"]
                syslog_message = ed["event"]
            else:
                del ed["_record"]
            if not getattr(record, "extra", None):
                record.extra = {}
            else:
                record.extra = record.extra.copy()
            record.extra.update(cleanup_event(ed))
            record.msg = ed["event"]
            # Discard args, they should have been rendered before by PositionalArgumentsFormatter
            record.args = ()

        if isinstance(full_message, dict):
            # routing logs from structlog logger
            # no need to execute prechain, only render the messages
            ed = full_message
            # Ensure to reinject the uuid, it may not have been injected in structlog (before
            # formatter that outputs to stdout/file does not want to)
            ed = processor_insert_application_data(None, None, ed)
            if not getattr(record, "extra", None):
                record.extra = {}
            else:
                record.extra = record.extra.copy()
            record.extra.update(cleanup_event(ed))
            record.msg = ed["event"]
            syslog_message = full_message["event"]
            full_message = syslog_message

        if fqdn:
            host = socket.getfqdn()
        elif localname:
            host = localname
        else:
            host = socket.gethostname()
        fields = {
            "version": "1.0",
            "host": host,
            "syslog_message": syslog_message,
            "full_message": full_message,
            "timestamp": record.created,
            "level": graypy.handler.SYSLOG_LEVELS.get(record.levelno, record.levelno),
            "facility": facility or record.name,
        }
        extra_to_recopy = [
            "uuid",
            "task_name",
            "application_name",
            "application_version",
            "application_flavor",
            "application_fingerprint",
        ]
        for e in extra_to_recopy:
            t = getattr(record, "extra", {}).get(e)
            if t:
                fields["_{}".format(e)] = t
                del record.extra[e]

        levelname = getattr(record, "extra", {}).get("level")
        if levelname:
            fields["levelname"] = levelname
            del record.extra["level"]

        if record.exc_info:
            fields["exc_info"] = [
                hide_unwanted_strings(str(s)) for s in record.exc_info
            ]

        if level_names:
            fields["level_name"] = logging.getLevelName(record.levelno)

        if facility is not None:
            fields.update({"_logger": record.name})

        if index is not None:
            fields.update({"index": index})

        if debugging_fields:
            fields.update(
                {
                    "file": record.pathname,
                    "line": record.lineno,
                    "_function": record.funcName,
                    "_pid": record.process,
                    "_thread_name": record.threadName,
                }
            )
            fields["_process_name"] = g_process_name

        self._inject_context(fields)

        if extra_fields:
            fields = self.add_extra_fields(fields, record)

        if "_extra" in fields:
            self._flatten_extra_if_needed(fields)

        if "_stack_info" in fields:
            if not fields["_stack_info"]:
                del fields["_stack_info"]

        return fields

    def _flatten_extra_if_needed(self, fields):
        values = {}
        if self.flat_extra and isinstance(fields["_extra"], dict):
            for k, v in fields["_extra"].items():
                if isinstance(v, (int, str, float, numbers.Real)) or v is None:
                    values[k] = v
            fields["_extra"] = {
                "flat": self.flat_extra_sep.join(
                    sorted("{}: {!r}".format(k, v) for k, v in fields["_extra"].items())
                )
            }
        # Create the `extra.values` dictionary with simple values (no dict, no set, no object, ...)
        if values:
            fields["_extra"]["values"] = values

    @staticmethod
    def add_extra_fields(message_dict, record):
        # skip_list is used to filter additional fields in a log message.
        # It contains all attributes listed in
        # http://docs.python.org/library/logging.html#logrecord-attributes
        # plus exc_text, which is only found in the logging module source,
        # and id, which is prohibited by the GELF format.
        skip_list = [
            "args",
            "asctime",
            "created",
            "exc_info",
            "exc_text",
            "filename",
            "funcName",
            "id",
            "levelname",
            "levelno",
            "lineno",
            "module",
            "msecs",
            "message",
            "msg",
            "name",
            "pathname",
            "process",
            "processName",
            "relativeCreated",
            "thread",
            "threadName",
        ]
        # Also removing the keys that has been copied to root level
        skip_extra_list = [
            "event",
            "level",
            "logger",
            "timestamp",
            "_record",
            "msg",
            "extra",
        ]
        promote_extra_lst = [
            "stack_info",
            "application_name",
            "application_version",
            "application_flavor",
            "application_fingerprint",
        ]

        for key, value in record.__dict__.items():
            if key == "extra":
                for k, v in value.items():
                    if k == "extra":
                        for kk, vv in v.items():
                            message_dict.setdefault("_extra", {})[
                                kk
                            ] = hide_unwanted_strings(vv)
                    elif k in promote_extra_lst:
                        message_dict["_{}".format(k)] = hide_unwanted_strings(v)
                    elif k not in skip_extra_list:
                        message_dict.setdefault("_extra", {})[
                            k
                        ] = hide_unwanted_strings(v)
            elif not key.startswith("_"):
                if key in promote_extra_lst:
                    message_dict["_{}".format(key)] = hide_unwanted_strings(value)
                elif key not in skip_list:
                    message_dict.setdefault("_extra", {})[key] = hide_unwanted_strings(
                        value
                    )
        return message_dict

    def _inject_context(self, record):
        d = g_context_dict
        if d:
            record.setdefault("extra", {}).update(d)
