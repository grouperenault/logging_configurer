# -*- coding: utf-8 -*-

# pylint: disable=too-many-locals,too-many-branches,too-few-public-methods

# Standard Library
from typing import Optional

# Logging Configurer Modules
from logging_configurer import context

RESERVED_KEYS = ("name", "id")
g_uuid = {"uuid": None, "task_name": None}
g_application_info = {
    "name": None,
    "flavor": None,
    "version": None,
    "fingerprint": None,
}


def cleanup_key(k):
    if k in RESERVED_KEYS:
        k = "_" + k
    return k


def get_uuid() -> Optional[str]:
    """
    Return the current UUID of this logging session.
    """

    return g_uuid["uuid"]


def cleanup_event(d):
    if isinstance(d, dict):
        return {cleanup_key(k): cleanup_event(v) for k, v in d.items()}
    if isinstance(d, (int, float)):
        return d
    if isinstance(d, (list, tuple)):
        types = {type(x) for x in d}
        if len(types) > 1:
            return (
                "Logging value cleanup Value exception: "
                "all elements of a list must be of the same type: {} {}".format(
                    types, d
                )
            )
        return [cleanup_event(x) for x in d]
    return str(d)


def wrap_for_logging_formatter(_logger, _name, event_dict):
    event_dict = cleanup_event(event_dict)
    return {"extra": event_dict, "msg": event_dict["event"]}


def processor_insert_application_data(_1, _2, event_dict):
    if context.is_initialized():
        if event_dict:
            event_dict.update(context.get_copy())
        else:
            event_dict = context.get_copy()

    if g_uuid["uuid"]:
        if event_dict:
            event_dict["uuid"] = g_uuid["uuid"]
        else:
            event_dict = {"uuid": g_uuid["uuid"]}
    if g_uuid["task_name"]:
        event_dict["task_name"] = g_uuid["task_name"]
    if g_application_info["name"]:
        event_dict["application_name"] = g_application_info["name"]
    if g_application_info["flavor"]:
        event_dict["application_flavor"] = g_application_info["flavor"]
    if g_application_info["version"]:
        event_dict["application_version"] = g_application_info["version"]
    if g_application_info["fingerprint"]:
        event_dict["application_fingerprint"] = g_application_info["fingerprint"]
    return event_dict
