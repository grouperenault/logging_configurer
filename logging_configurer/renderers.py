# -*- coding: utf-8 -*-

# Standard Library
import numbers
from typing import Any
from typing import Dict

# Third Party Libraries
import structlog
from six import StringIO

# pylint: disable=too-many-locals,too-many-branches,too-few-public-methods

log = structlog.get_logger("logging_configurer")

g_unwanted_strings_in_logs = []
g_context_dict = {}


def reset_unwanted_strings():
    g_unwanted_strings_in_logs.clear()


def add_unwanted_string_in_structlog(unwanted_string):
    global g_unwanted_strings_in_logs  # pylint: disable=W0603
    if not unwanted_string:
        return
    unwanted_string = unwanted_string.strip()
    if not unwanted_string:
        return
    if unwanted_string not in g_unwanted_strings_in_logs:
        g_unwanted_strings_in_logs.append(unwanted_string)


def hide_unwanted_strings(  # pylint: disable=too-many-return-statements
    leaker: Any,
) -> Any:
    if not leaker:
        return leaker
    if isinstance(leaker, numbers.Integral):
        return leaker
    if isinstance(leaker, numbers.Number):
        return leaker
    if isinstance(leaker, bool):
        return leaker
    if isinstance(leaker, dict):
        return {
            hide_unwanted_strings(k): hide_unwanted_strings(v)
            for k, v in leaker.items()
        }
    if isinstance(leaker, set):
        return {hide_unwanted_strings(v) for v in leaker}
    if isinstance(leaker, list):
        return [hide_unwanted_strings(v) for v in leaker]

    str_leaker = str(leaker)
    clean_leaker = str_leaker
    for unwanted in g_unwanted_strings_in_logs:
        clean_leaker = clean_leaker.replace(unwanted, "******")
    if clean_leaker != str_leaker:
        return clean_leaker
    return leaker


def add_context_to_gelf_output(context_dict: Dict[Any, Any]):
    global g_context_dict  # pylint: disable=W0603
    if not context_dict:
        return
    g_context_dict.update(context_dict)


def shorten_level(level):
    m = {
        "critical": "critic",
        "exception": "except",
        "error": "error",
        "warn": "warn",
        "warning": "warn",
        "info": "info",
        "debug": "debug",
        "notset": "notset",
    }
    return m.get(level, "notset")


MAX_LEVEL_SIZE = 6


class PlainRenderer(object):
    def __call__(self, _, __, event_dict):
        line = []

        event = event_dict.pop("event")
        if event_dict:
            line.append(event)

        return " ".join(line)


class ConsoleRenderer(structlog.dev.ConsoleRenderer):
    def __init__(
        self,
        *args,
        colors=False,
        pad_loggername=35,
        pad_timestamp=23,
        pad_event=None,
        **kwargs
    ):
        """
        Contruct a ConsoleRenderer.

        Arguments:
            pad_loggername: if set to 0, disable the "loggername" column

        """
        # pylint: disable=bare-except,import-error,unused-variable
        try:
            import colorama  # noqa: F401
        except:  # noqa: E722
            # No colorama support found, disable color
            colors = False
            colorama = None
        # pylint: enable=bare-except,import-error,unused-variable
        super(ConsoleRenderer, self).__init__(
            pad_event=None, colors=colors, *args, **kwargs
        )
        self.pad_timestamp = pad_timestamp
        self.pad_loggername = pad_loggername

        # pylint: disable=protected-access
        if self._styles == structlog.dev._ColorfulStyles and colorama:
            # Overwriting colors
            self._level_to_color["critical"] = (
                colorama.Fore.LIGHTMAGENTA_EX + colorama.Style.BRIGHT
            )
            self._level_to_color["debug"] = colorama.Fore.CYAN + colorama.Style.BRIGHT
            #     "critical": styles.level_critical,
            #     "exception": styles.level_exception,
            #     "error": styles.level_error,
            #     "warn": styles.level_warn,
            #     "warning": styles.level_warn,
            #     "info": styles.level_info,
            #     "debug": styles.level_debug,
            #     "notset": styles.level_notset,
            # }
        # pylint: enable=protected-access

    def __call__(self, _, __, event_dict):  # noqa: C901
        # pylint: disable=too-many-statements
        # using StringIO for performance
        sio = StringIO()
        ln = 0

        ts = event_dict.pop("timestamp", None)
        if ts is not None:
            sts = str(ts)
            if self.pad_timestamp:
                sts = "{0:.{width}}".format(sts, width=self.pad_timestamp)
            ln += len(sts) + 1
            sio.write(
                # can be a number if timestamp is UNIXy
                self._styles.timestamp
                + sts
                + self._styles.reset
                + " "
            )
        level = event_dict.pop("level", None)
        if level is not None:
            # pylint: disable=W0212
            padded_level = structlog.dev._pad(
                shorten_level(level.lower()).upper(), MAX_LEVEL_SIZE
            )
            ln += len(padded_level) + 2
            sio.write(
                self._level_to_color[level] + padded_level + self._styles.reset + "| "
            )
            # pylint: enable=W0212

        logger_name = event_dict.pop("logger", None)
        if logger_name is not None and self.pad_loggername:
            logger_name = "{0:<{width}}".format(
                logger_name[: self.pad_loggername], width=self.pad_loggername
            )
            ln += len(logger_name) + 3
            sio.write(
                self._styles.logger_name
                + self._styles.bright
                + logger_name
                + self._styles.reset
                + " | "
            )

        event_line_padding = ln
        event = event_dict.pop("event")
        if event_dict:
            if self._pad_event:
                padded_event = "{0:<{w}}".format(
                    event[: self._pad_event], w=self._pad_event
                )
                ln += len(padded_event) + 1
            else:
                padded_event = event + "  "
                ln += len(padded_event) + 1
            event = padded_event + self._styles.reset + " "
        else:
            event += self._styles.reset
        sio.write(self._styles.bright + event)

        stack = event_dict.pop("stack", None)
        exc = event_dict.pop("exception", None)

        fl = True
        for k, v in sorted(event_dict.items()):
            if not fl:
                sio.write("\n" + " " * ln)
            sio.write(
                self._styles.kv_key
                + k
                + self._styles.reset
                + "="
                + self._styles.kv_value
                + self._repr(v)
                + self._styles.reset
            )
            fl = False
        if exc:
            if isinstance(exc, (Exception, BaseException)):
                exc = repr(exc)
            else:
                exc = str(exc)
                if exc.startswith("Traceback (most recent call last)"):
                    exc_fistln, _, exc_rest = exc.partition("\n")
                    if exc_rest is None:
                        exc_rest = []
                    else:
                        exc_rest = exc_rest.split("\n")
                    exc_fistln = (
                        self._styles.bright
                        + self._styles.level_exception
                        + exc_fistln
                        + self._styles.reset
                    )
                    exc = "\n".join([exc_fistln] + exc_rest)
            exc = hide_unwanted_strings(exc)
        if stack is not None:
            stack = hide_unwanted_strings(stack)
            stack_fistln, _, stack_rest = stack.partition("\n")
            if stack_rest is None:
                stack_rest = []
            else:
                stack_rest = stack_rest.split("\n")
            self._pad_line(
                sio,
                event_line_padding,
                "\n".join(
                    [
                        self._styles.bright
                        + self._styles.level_exception
                        + stack_fistln
                        + self._styles.reset
                    ]
                    + stack_rest
                ),
            )
            if exc is not None:
                self._pad_line(
                    sio,
                    event_line_padding,
                    "=" * 79
                    + "\n"
                    + self._styles.bright
                    + self._styles.level_exception
                    + "Exception: "
                    + self._styles.reset
                    + exc,
                )
        elif exc is not None:
            self._pad_line(sio, event_line_padding, exc)
        return sio.getvalue()
        # pylint: enable=too-many-statements

    @staticmethod
    def _pad_line(sio, pad, string, new_line_first=True):
        splitted_str = str(string).split("\n")
        if new_line_first:
            for line in splitted_str:
                sio.write("\n" + " " * pad + line)
        else:
            sio.write("\n".join([" " * pad + line for line in splitted_str]))
