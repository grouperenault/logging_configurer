# -*- coding: utf-8 -*-

# Standard Library
import asyncio
import random
import sys

# Third Party Libraries
import pytest

# Logging Configurer Modules
from logging_configurer import context

# pylint: disable=redefined-outer-name,protected-access


@pytest.fixture
def c():
    yield context._renew()


@pytest.mark.skipif(sys.version_info < (3, 7), reason="requires python3.7 or higher")
def test_type(c):  # pylint: disable=unused-argument
    context.update({"repas": "carottes"})
    with pytest.raises(ValueError):
        context.update("toto")


@pytest.mark.skipif(sys.version_info < (3, 7), reason="requires python3.7 or higher")
def test_basic_set(c):  # pylint: disable=unused-argument
    d = {"dick": "rivers"}
    context.update(d)
    assert context.get_copy() == d
    del context.get_copy()["dick"]
    assert context.get_copy() == d


async def task_empty(d, p={}):  # pylint: disable=dangerous-default-value
    assert {**p, **d} == context.get_copy()
    print("on async task:", context.get_copy())


async def coro_updating_context():
    d = {1: 1}
    t = context.update(d)
    assert d == context.get_copy()
    print("on async task:", context.get_copy())
    return t


global_array = []


async def subcoro(name):
    c = context.get_copy()
    global_array.append((name, c))


async def coro(name, number):
    context.update({"name": name, "number": number})
    await subcoro(name)
    await asyncio.sleep(0.2 - number)
    await subcoro(name)
    assert {"name": name, "number": number} == context.get_copy()


@pytest.mark.asyncio
@pytest.mark.skipif(sys.version_info < (3, 7), reason="requires python3.7 or higher")
async def test_ctx_on_gather(c):  # pylint: disable=unused-argument
    # Schedule three calls *concurrently*:
    await asyncio.gather(
        coro("A", 0.0), coro("B", 0.1), coro("C", 0.2),
    )

    assert global_array == [
        ("A", {"name": "A", "number": 0.0}),
        ("B", {"name": "B", "number": 0.1}),
        ("C", {"name": "C", "number": 0.2}),
        ("C", {"name": "C", "number": 0.2}),
        ("B", {"name": "B", "number": 0.1}),
        ("A", {"name": "A", "number": 0.0}),
    ]


@pytest.mark.asyncio
@pytest.mark.skipif(sys.version_info < (3, 7), reason="requires python3.7 or higher")
async def test_ctx_on_coro_only(c):  # pylint: disable=unused-argument
    assert context.get_copy() == {}
    t = await coro_updating_context()
    # Need to reset because no task have been created to run the coro,
    # current contextvar has been changed by the coro code
    context.reset(t)
    assert context.get_copy() == {}


@pytest.mark.asyncio
@pytest.mark.skipif(sys.version_info < (3, 7), reason="requires python3.7 or higher")
async def test_ctx_on_created_coro(c):  # pylint: disable=unused-argument
    assert context.get_copy() == {}
    task = asyncio.create_task(coro_updating_context())  # pylint: disable=no-member
    await task
    # No need to reset the context because a new task has been created
    # and it inherit the context
    assert context.get_copy() == {}


async def concurrent_coro_updating_context(d):
    await asyncio.sleep(random.uniform(0, 0.2))  # nosec
    assert d == context.get_copy()
    inner = {1: asyncio.current_task()._repr_info}  # pylint: disable=no-member
    t = context.update(inner)
    await asyncio.sleep(random.uniform(0, 0.2))  # nosec
    assert {**d, **inner} == context.get_copy()
    print("on async task:", context.get_copy())
    return t


@pytest.mark.asyncio
@pytest.mark.skipif(sys.version_info < (3, 7), reason="requires python3.7 or higher")
async def test_ctx_on_concurrent_coros(c):  # pylint: disable=unused-argument
    d = {"first": 1, "second": "2", "third": "trois"}
    context.update(d)
    assert d == context.get_copy()
    coros = []
    for _i in range(10):
        coros.append(
            asyncio.create_task(  # pylint: disable=no-member
                concurrent_coro_updating_context(d)
            )
        )
    await asyncio.gather(*coros)
    print("after coros:", context.get_copy())
    assert d == context.get_copy()
