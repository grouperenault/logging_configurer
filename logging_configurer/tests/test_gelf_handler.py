# -*- coding: utf-8 -*-

# Third Party Libraries
import pytest

# Logging Configurer Modules
from logging_configurer.customized_gelf_handler import MyGELFHandler

# pylint: disable=redefined-outer-name,protected-access


@pytest.fixture
def gh():

    i = {
        "_function": "_proxy_to_logger",
        "_pid": 20241,
        "_process_name": "ex_gelf.py",
        "_thread_name": "MainThread",
        "_uuid": "500d324c-b17c-400a-9f91-097009b8cdf2",
        "facility": "_common",
        "file": "/path/to/python/file.py",
        "full_message": "[logging  ] param and extra",
        "host": "tldlab020",
        "level": 4,
        "levelname": "warning",
        "line": 177,
        "syslog_message": "[logging  ] param and extra",
        "timestamp": 1521036943.4685009,
        "version": "1.0",
    }
    gh = MyGELFHandler([], "fakehost", 1234)

    yield i, gh


def gh_with_index():
    i = {
        "_function": "_proxy_to_logger",
        "_pid": 20241,
        "_process_name": "ex_gelf.py",
        "_thread_name": "MainThread",
        "_uuid": "500d324c-b17c-400a-9f91-097009b8cdf2",
        "facility": "_common",
        "file": "/path/to/python/file.py",
        "full_message": "[logging  ] param and extra",
        "host": "tldlab020",
        "level": 4,
        "levelname": "warning",
        "line": 177,
        "syslog_message": "[logging  ] param and extra",
        "timestamp": 1521036943.4685009,
        "version": "1.0",
    }
    gh = MyGELFHandler([], "fakehost", 1234, index="logstash")
    yield i, gh


def make_record(d, mocker):
    r = mocker.Mock()
    r.__dict__ = {
        "filename": "_base.py",
        "stack_info": None,
        "process": 24242,
        "thread": 139994951538432,
        "relativeCreated": 83.24241638183594,
        "processName": "MainProcess",
        "levelname": "WARNING",
        "_name": "warning",
        "msg": "[logging  ] param and extra",
        "args": (),
        "funcName": "_proxy_to_logger",
        "pathname": "/path/to/python.py",
        "created": 1521037672.4098852,
        "name": "_common",
        "lineno": 177,
        "threadName": "MainThread",
        "module": "_base",
        "exc_text": None,
        "exc_info": None,
        "_logger": " <logging.Logger object at 0x7f531bf14438>",
        "msecs": 409.8851680755615,
        "levelno": 30,
    }
    r.__dict__.update(d)
    return r


def test_add_extra_fields_from_logging(gh, mocker):
    i, gh = gh
    r = make_record(
        {
            "extra": {
                "timestamp": "2018-03-14 14:27:52",
                "logger": "_common",
                "extra": {"val": "123", "val_in_extra": "value",},
                "event": "[logging  ] param and extra",
            },
            "lazy_param_dict": {"a": "a_value",},
        },
        mocker,
    )

    o = gh.add_extra_fields(i, r)
    assert o == {
        "_function": "_proxy_to_logger",
        "_pid": 20241,
        "_process_name": "ex_gelf.py",
        "_thread_name": "MainThread",
        "_uuid": "500d324c-b17c-400a-9f91-097009b8cdf2",
        "facility": "_common",
        "file": "/path/to/python/file.py",
        "full_message": "[logging  ] param and extra",
        "host": "tldlab020",
        "level": 4,
        "levelname": "warning",
        "line": 177,
        "syslog_message": "[logging  ] param and extra",
        "timestamp": 1521036943.4685009,
        "version": "1.0",
        "_extra": {
            "lazy_param_dict": {"a": "a_value",},
            "val": "123",
            "val_in_extra": "value",
        },
        "_stack_info": None,
    }


def test_add_extra_fields_flatten(gh, mocker):
    i, gh = gh
    gh.set_flat_extra()
    r = make_record(
        {
            "extra": {
                "timestamp": "2018-03-14 14:27:52",
                "logger": "_common",
                "extra": {"val": "123", "val_in_extra": "value"},
                "event": "[logging  ] param and extra",
            },
            "lazy_param_dict": {"a": "a_value"},
        },
        mocker,
    )

    o = gh.add_extra_fields(i, r)
    gh._flatten_extra_if_needed(o)
    assert o == {
        "_function": "_proxy_to_logger",
        "_pid": 20241,
        "_process_name": "ex_gelf.py",
        "_thread_name": "MainThread",
        "_uuid": "500d324c-b17c-400a-9f91-097009b8cdf2",
        "facility": "_common",
        "file": "/path/to/python/file.py",
        "full_message": "[logging  ] param and extra",
        "host": "tldlab020",
        "level": 4,
        "levelname": "warning",
        "line": 177,
        "syslog_message": "[logging  ] param and extra",
        "timestamp": 1521036943.4685009,
        "version": "1.0",
        "_extra": {
            "flat": "lazy_param_dict: {'a': 'a_value'}\nval: '123'\nval_in_extra: 'value'",
            "values": {"val": "123", "val_in_extra": "value",},
        },
        "_stack_info": None,
    }
