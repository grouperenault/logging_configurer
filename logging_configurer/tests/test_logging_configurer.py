# -*- coding: utf-8 -*-
"""
test_logging_configurer
----------------------------------
Tests for `logging_configurer` module.
"""

# Standard Library
import logging

# Third Party Libraries
import pytest

# Logging Configurer Modules
from logging_configurer._logging_configurer import ConsoleRenderer
from logging_configurer._logging_configurer import LoggingConfigurer
from logging_configurer._structlog_logger import get_logger

# pylint: disable=redefined-outer-name, unused-argument, protected-access


@pytest.fixture
def setup_hidden_password():
    logger_conf = LoggingConfigurer()
    logger_conf.set_debug(True)
    logger_conf.set_output_type("dev")
    logger_conf.apply()

    from structlog._config import _CONFIG

    def check_hidden(_1, _2, event):
        assert "MYPASSWORD" not in event["event"]
        return event

    _CONFIG.default_processors.insert(len(_CONFIG.default_processors) - 1, check_hidden)
    log = get_logger()
    yield log
    LoggingConfigurer.reset_unwanted_strings()


@pytest.fixture
def setup_logging():
    logger_conf = LoggingConfigurer()
    logger_conf.set_debug(True)
    logger_conf.set_output_type("dev")
    logger_conf.apply()
    log = get_logger()
    yield log


def test_hidden_password(setup_hidden_password):
    log = setup_hidden_password
    LoggingConfigurer.hide_unwanted_string("MYPASSWORD")
    log.debug("MYPASSWORD should be hidden")


def test_password_not_hidden(setup_hidden_password):
    log = setup_hidden_password
    with pytest.raises(AssertionError):
        log.debug("MYPASSWORD should not be hidden")


def test_hidden_password_in_excep(setup_hidden_password):
    log = setup_hidden_password
    try:
        raise Exception("exception with hidden string - MYPASSWORD")
    except Exception:
        log.exception("exception with string hidden")


def test_exception(setup_logging, caplog):
    log = setup_logging
    try:
        raise Exception("an exception")
    except Exception:
        log.debug("an exception occured")
        log.exception("this exception is now printed")
    captured = caplog.text
    assert "this exception is now printed" in captured


def test_consolerenderer_exception_on_multilines():
    rend = ConsoleRenderer()
    ss = rend(
        None,
        None,
        event_dict={
            "event": "this is an exception event",
            "level": "error",
            "timestamp": "2017-12-27 15:19:08",
            "logger": "the.logger.name",
            "exception": (
                "Traceback (most recent call last):\n"
                '  File "/path/to/module/tests.py", line 64, in test_exception\n'
                '    raise Exception("an exception")\nException: an exception'
            ),
        },
    )
    wanted_output = [
        "2017-12-27 15:19:08 ERROR | the.logger.name                     | "
        + "this is an exception event   ",
        " " * 66 + "Traceback (most recent call last):",
        " " * 66 + '  File "/path/to/module/tests.py", line 64, in test_exception',
        " " * 66 + '    raise Exception("an exception")',
        " " * 66 + "Exception: an exception",
    ]
    assert ss == "\n".join(wanted_output)


def test_log_structlog(setup_logging, caplog):
    log = setup_logging
    log.debug(
        "a debug event",
        val1=1,
        val2=2,
        valdict={"k1": "v1", "k2": {"k2k1": "k2v1", "k2k2": "k2v2"}},
    )
    captured = caplog.text
    assert "a debug event" in captured


def test_log_extra(setup_logging, caplog):
    logginglog = logging.getLogger("a.logging.logger")
    logginglog.debug("a logging log message lazyval=%s", "val", extra={"k1": "v1"})
    captured = caplog.text
    assert "a logging log message lazyval=val" in captured


def test_consolerenderer_values():
    rend = ConsoleRenderer(colors=False)
    ss = rend(
        None,
        None,
        event_dict={
            "event": "a debug event",
            "timestamp": "2017-12-27 15:30:36",
            "val2": 2,
            "logger": "the.logger.name",
            "val1": 1,
            "level": "debug",
        },
    )

    wanted_output = [
        "2017-12-27 15:30:36 DEBUG | the.logger.name                     | a debug event   val1=1",
        "                                                                                  val2=2",
    ]
    assert ss == "\n".join(wanted_output)


def test_wait_for_gelf_not_enabled(mocker):
    mocker.patch("time.sleep")
    conf = LoggingConfigurer()
    assert conf.wait_for_gelf(timeout=1)


def test_wait_for_gelf_timeout(mocker):
    mocker.patch("time.sleep")
    conf = LoggingConfigurer()
    conf._gelf_enabled = True
    conf._gelf_host = "nawak"
    assert not conf.wait_for_gelf(timeout=2)


def test_wait_for_gelf_ok(mocker):
    mocker.patch("time.sleep")
    conf = LoggingConfigurer()
    conf._gelf_enabled = True
    conf._gelf_host = "127.0.0.1"
    assert conf.wait_for_gelf(timeout=1)
